package com.softtek.academy.spring.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.configuration.JDBCConfiguration;
import com.softtek.academy.spring.dao.PersonDao;
import com.softtek.academy.spring.repository.PersonRepository;
import com.softtek.academy.spring.services.PersonService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JDBCConfiguration.class })
@WebAppConfiguration
public class GetAllTest {

	@Qualifier("personRepository")
	@Autowired
	private PersonDao personDao;
	
	private List<Person> persons = new ArrayList<Person>();
	
	@Test
	public void testFindById() {
		Person p = new Person();
		p.setId(personDao.findById(1).getId());
		p.setName(personDao.findById(1).getName());
		p.setAge(personDao.findById(1).getAge());
		assertNotNull("No se pudo obtener", p);
	}
}
