<!DOCTYPE html>
<% String context = request.getContextPath(); %>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Menu Principal</title>
</head>
<body>
	<div class="container">
	  <a href="<%=context %>/person" class="list-group-item list-group-item-action">
	    Add New Person
	  </a>
	  <a href="<%=context %>/viewPerson" class="list-group-item list-group-item-action">Show Data Base</a>
	  <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
	  <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
	  <a href="#" class="list-group-item list-group-item-action	">Vestibulum at eros</a>
	</div>
</body>
</html>