package com.softtek.academy.spring.services;

import java.util.List;

import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.dao.PersonDao;

@Transactional
@Service
public class PersonServiceImpl implements PersonService{

	@Qualifier("personRepository")
	@Autowired
	private PersonDao personDao;
	
	@Override
	public List<Person> getAll() {
		return personDao.getAll();
	}

	@Override
	public void addNewPerson(Person person) {
		personDao.addNewPerson(person);
	}

	@Override
	public void deletePerson(int id) {
		System.out.println("Entre al deletePerson del ServiceImplementation");
		System.out.println("Id: " + id);
		personDao.deletePerson(id);
	}

	@Override
	public void updatePerson(Person person) {
		personDao.updatePerson(person);
	}

	@Override
	public Person finById(int id) {
		return personDao.findById(id);
	}

}
