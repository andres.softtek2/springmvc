package com.softtek.academy.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.dao.PersonDao;
import com.softtek.academy.spring.services.PersonService;

@Controller
public class PersonController {
	
	@Autowired
	PersonService personService;
	
	@RequestMapping(value="/person", method=RequestMethod.GET)
    public ModelAndView person() {
        return new ModelAndView("person", "command", new Person());
    }
   
    @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("SpringWeb")Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        //dao.save(person);		//CAMBIAR POR LA IMPLEMENTACION
        personService.addNewPerson(person);
        model.addAttribute("command", new Person());
       
        return "person";  
    }
    
    @RequestMapping(value = "/saveChanges", method = RequestMethod.POST)
    public String saveChanges(@ModelAttribute("SpringWeb")Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        personService.updatePerson(person);
        model.addAttribute("command", new Person());
        model.addAttribute("persons", personService.getAll());
        return "show";  
    }
    
   @RequestMapping(value = "/viewPerson", method = RequestMethod.GET)
    public String viewPerson(@ModelAttribute("SpringWeb")Person person, ModelMap model) {
    	System.out.println(personService.getAll());
    	model.addAttribute("persons", personService.getAll());
        return "show";  
    }
   
   @RequestMapping(value = "/deletePerson/{id}", method = RequestMethod.GET)
   public String deletePerson(@PathVariable int id, ModelMap model) {
	   System.out.println(personService.getAll());
	   personService.deletePerson(id);
	   model.addAttribute("persons", personService.getAll());
	   return "show";
   }
   
   @RequestMapping(value = "updatePerson/{id}", method = RequestMethod.GET)
   public String updatePerson(@PathVariable int id, ModelMap model) {
	   System.out.println(personService.getAll());
	   Person person = personService.finById(id);
	   model.addAttribute("id", person.getId());
	   model.addAttribute("name", person.getName());
	   model.addAttribute("age", person.getAge());
	   model.addAttribute("command", person);
	   return "update";
   }
}
