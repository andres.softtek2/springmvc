package com.softtek.academy.spring.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.softtek.academy.spring.beans.Person;

public interface PersonService {
	
	List<Person> getAll();

	void addNewPerson(Person person);

	void deletePerson(int id);

	void updatePerson(Person person);

	Person finById(int id);
}
