package com.softtek.academy.spring.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.mysql.cj.Session;
import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.dao.PersonDao;



@Repository("personRepository")
public class PersonRepository implements PersonDao{
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> getAll() {
		Query query = entityManager.createQuery("SELECT p FROM Person p", Person.class);
		return query.getResultList();
	}

	@Override
	public void addNewPerson(Person person) {
		entityManager.persist(person);
	}

	@Override
	public void deletePerson(int id) {
		Person p = entityManager.find(Person.class, id);
		entityManager.remove(p);
	}

	@Override
	public Person findById(int id) {
		return entityManager.find(Person.class, id);
	}

	@Override
	public void updatePerson(Person person) {
		person = entityManager.merge(person);
	}

}
