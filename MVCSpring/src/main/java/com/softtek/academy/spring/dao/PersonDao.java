package com.softtek.academy.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.softtek.academy.spring.beans.Person;

public interface PersonDao {

	public List<Person> getAll();

	public void addNewPerson(Person person);

	public void deletePerson(int id);

	public Person findById(int id);

	public void updatePerson(Person person);
	
	
	
	
	
	
	
	/*
	JdbcTemplate template;
	   
    public void setTemplate(JdbcTemplate template) {
        this.template = template;
    }
   
    public int save(Person p) {
        String sql="INSERT INTO person (name,age,id) VALUES ('"+p.getName()+"',"+p.getAge()+",'"+p.getId()+"');";
        return template.update(sql);
    }
   
    public int update(Person p){
        String sql="update person set name='"+p.getName()+"', age="+p.getAge()+",id='"+p.getId()+"' where id="+p.getId()+"";
        return template.update(sql);
    }
   
    public int delete(int id){
        String sql="delete from person where id="+id+"";
        return template.update(sql);
    }
   
    public Person getEmpById(int id){
        String sql="select * from person where id=?";
        return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Person>(Person.class));
    }
   
    public List<Person> getPersons(){
        return template.query("select * from person",new RowMapper<Person>(){
            public Person mapRow(ResultSet rs, int row) throws SQLException {
                Person e=new Person();
                e.setId(rs.getInt(3));
                e.setName(rs.getString(1));
                e.setAge(rs.getInt(2));
                return e;
            }
        });
    } */

}
